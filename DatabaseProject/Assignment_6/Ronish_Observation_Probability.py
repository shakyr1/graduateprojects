# Assignment 6
# Observation Probability
# Ronish Shakya

class HMM:
    states = []
    symbols = []
    string_of_symbols = ""
    start_p = []
    trans_p = []
    emit_p = []
    alpha = []


    def __init__(self, string_of_symbols, states, symbols, initP, transP, emitP):
        self.string_of_symbols = string_of_symbols
        self.states = states.split(' ')
        self.symbols = symbols.split(' ')
        self.start_p = initP.split(' ')
        self.setTransitionProbabilites(transP)
        self.setEmissionProbabilites(emitP)


    def setTransitionProbabilites(self, t):
        """Transition Probabilities"""
        t = t.split(' ')
        stateLen = len(self.states)
        for i in range(0, stateLen):
            self.trans_p.append(t[i*stateLen:(i+1)*stateLen])


    def setEmissionProbabilites(self, e):
        """Emission probabilities"""
        symbolLen = len(self.symbols)
        e = e.split(' ')
        for i in range(0, len(self.states)):
            self.emit_p.append(e[i*symbolLen:(i+1)*symbolLen])

    def calculateAlphaOne(self):
        """Calculate alpha for the first character of the string"""
        if (len(self.start_p) > 0 and len(self.emit_p) > 0):
            for i in range(0, len(self.states)):
                self.alpha.append(float(self.start_p[i]) * float(self.emit_p[i][0]))
            print "alpha1\t:", self.alpha

    def calculateAlpha(self):
        """Calculate the values of alpha from the 2nd character to the end of the string."""
        for c in range(1, len(self.string_of_symbols)):
            temp_alpha = []
            for j in range(0, len(self.states)):
                temp = 0
                for i in range(0, len(self.states)):
                    temp += float(self.trans_p[i][j]) * float(self.emit_p[j][self.symbols.index(self.string_of_symbols[c])]) * float(self.alpha[i])
                temp_alpha.append(temp)
            print "alpha%d\t:" %(c+1), temp_alpha
            self.alpha = temp_alpha
        return self.alpha

    def calculateObservationProbability(self):
        print "String: ", self.string_of_symbols
        print "Initial Probabilities: ", self.start_p, "\nEmission Probabilities: " , self.emit_p
        print "Transition Probabilities: ", self.trans_p
        self.calculateAlphaOne()
        self.calculateAlpha()
        print "Observation Probability: ", sum(self.alpha)

# input data
s = "xzyyzzyzyy"
states = "A B"
symbols = "x y z"
initP = "0.5 0.5"
transP = "0.303 0.697 " \
        "0.831 0.169"
emisP = "0.533 0.065 0.402 " \
        "0.342 0.334 0.324"


hmmObj = HMM(s, states, symbols, initP, transP, emisP)
hmmObj.calculateObservationProbability()