#Ronish Shakya
#Assignment 3
import json
from nltk.corpus import stopwords
from nltk.tokenize import wordpunct_tokenize
from nltk.stem.porter import PorterStemmer
from itertools import groupby

with open("cran.all.1400", 'rt') as in_file:  # Open file for reading text data
    whole_doc = in_file.read()

porter = PorterStemmer()
print "\nRonish Shakya"
print "Assignment #3"
print "*************\n"

inverted_index = {}

for id in range(1,1400):
    begin_pos = '.I ' + str(id)
    final_pos = '.I ' + str(id + 1)
    data = whole_doc[whole_doc.find(begin_pos) + len(''):whole_doc.find(final_pos)].strip()
    data = data[data.find('.W') + len(''):data.find('.I ' + final_pos)].strip()
    # remove stop words
    data = data.replace(".W", "")
    stop_words = set(stopwords.words('english'))
    stop_words.update(['.', ',', '"', "'", '?', '!', ':', ';', '(', ')', '[', ']', '{', '}', '/', '-'])
    list_of_words = [porter.stem(i.lower()) for i in wordpunct_tokenize(data) if i.lower() not in stop_words if
                     not i.isdigit() if i.isalpha()]

    # calculating no. of unique terms
    word_set = set(list_of_words)

    # finding the term frequency

    freq_dict = dict((key, len(list(group))) for key, group in groupby(sorted(list_of_words)))
    for k in sorted(freq_dict):
        if k not in inverted_index:
            inverted_index[k] = [1, [[id, freq_dict[k]]]]
        else:
            inverted_index[k][0] += 1
            inverted_index[k][1].append([id, freq_dict[k]])

    print "\n"

# writing inverted index into an external file
temp_json = json.dumps(inverted_index)
with open("Output", "w") as fp:
    row = 'INV_FILE_HASH =\n'
    fp.write(row)
    fp.write(temp_json)

print "The inverted index has been written to a file!"


