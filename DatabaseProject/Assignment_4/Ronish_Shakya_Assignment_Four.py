#Ronish Shakya
#Assignment 4
import ast
from nltk.stem.porter import PorterStemmer
from pythonds.basic.stack import Stack

with open('Output', 'r') as myfile:
    data=myfile.read()

data = data.replace('INV_FILE_HASH =','')

inverted_index = ast.literal_eval(data)
porter = PorterStemmer()

def infixToPostfix(token_list):
    prec = {}
    prec["not"] = 3
    prec["and"] = 2
    prec["or"] = 2
    prec["("] = 1
    opStack = Stack()
    postfixList = []
    tokenList = token_list

    for token in tokenList:
        if token == '(':
            opStack.push(token)
        elif token == ')':
            topToken = opStack.pop()
            while topToken != '(':
                postfixList.append(topToken)
                topToken = opStack.pop()
        elif token == 'not' or token == 'and' or token == 'or':
            while (not opStack.isEmpty()) and \
               (prec[opStack.peek()] >= prec[token]):
                  postfixList.append(opStack.pop())
            opStack.push(token)
        else:
            postfixList.append(token)

    while not opStack.isEmpty():
        postfixList.append(opStack.pop())
    return " ".join(postfixList)

# print(infixToPostfix("vary AND user"))
# print(infixToPostfix("panama OR NOT user"))
# print(infixToPostfix("four AND solid OR consequ AND NOT circuitri"))
# print(infixToPostfix("panama OR user AND vary"))


def postfixEval(postfixExpr):
    operandStack = Stack()
    tokenList = postfixExpr.split()

    for token in tokenList:

        if token == 'and':
            #do and operation
            operand2 = operandStack.pop()
            operand1 = operandStack.pop()
            result = doAnd(operand1,operand2)
            operandStack.push(result)

        elif token == 'or':
            #do or operation
            operand2 = operandStack.pop()
            operand1 = operandStack.pop()
            result = doOr(operand1, operand2)
            operandStack.push(result)

        elif token == 'not':
            #do NOT operation
            operand_temp = operandStack.pop()
            result = doNot(operand_temp)
            operandStack.push(result)

        else:
            doc_list=[]
            #operand. push to the stack
            if token in inverted_index:
                operand = inverted_index[token]
                for i in range(0, operand[0]):
                    temp = operand[1][i][0]
                    doc_list.append(temp)
            operandStack.push(doc_list)

    return operandStack.pop()

def doNot(operand):
    resdoclist = []
    for i in range(1,1401):
        if i not in operand:
            resdoclist.append(i)
    return resdoclist



def doAnd(operand1,operand2):
    resdoclist = []
    doclist_1 = operand1
    doclist_2 = operand2

    p1 = 0
    p2 = 0

    length1 = len(doclist_1)
    length2 = len(doclist_2)

    while p1 != length1 and p2 != length2:
        if doclist_1[p1] == doclist_2[p2]:
            resdoclist.append(doclist_1[p1])
            p1 += 1
            p2 += 1

        elif doclist_1[p1] < doclist_2[p2]:
            p1 += 1

        else:
            p2 += 1

    return resdoclist


def doOr(operand1,operand2):
    resdoclist = []
    doclist_1 = operand1
    doclist_2 = operand2

    length1 = len(doclist_1)
    length2 = len(doclist_2)

    for j in range(0,length1):
        if doclist_1[j] not in resdoclist:
            resdoclist.append(doclist_1[j])

    for k in range(0,length2):
        if doclist_2[k] not in resdoclist:
            resdoclist.append(doclist_2[k])

    return sorted(resdoclist)

# postfixRes = infixToPostfix("panama OR user AND vary")
# postfixRes = infixToPostfix("four AND upstream AND micromanomet")
# postfixRes = infixToPostfix("four AND solid")
# postfixRes = infixToPostfix("four AND solid OR consequ")
# postfixRes = infixToPostfix("four AND ( solid OR consequ ) AND NOT circuitri")
query = raw_input("Enter a query: ")
token_list = []
for word in query.split():
    word = porter.stem(word.lower())
    token_list.append(word)

postfixRes = infixToPostfix(token_list)
# print postfixRes
finalres = postfixEval(postfixRes)

print "List of Document Ids: "+str(finalres)


