#Ronish Shakya
#Assignment 7

import re
import random
import string
import numpy as np
import Hmm

class SpellingCorrector:
    training_set = []
    testing_set = []
    corrupted_training_set = []
    corrupted_testing_set = []

    mistake_characters = {
        'a': ['q', 'w', 's', 'x', 'z', 'e', 'd'],
        'b': ['f', 'g', 'h', 'j', 'n', 'v', 'k', 'c', 'm'],
        'c': ['x', 's', 'd', 'f', 'v', 'g', 'b', 'z', 'a'],
        'd': ['q', 'w', 'e', 'r', 's', 't', 'f', 'x', 'c', 'v'],
        'e': ['w', 'r', 's', 'd', 'f', 'q', 't', 'a'],
        'f': ['e', 'r', 't', 'd', 'g', 'c', 'v', 'b', 'y', 'h'],
        'g': ['r', 't', 'y', 'f', 'h', 'v', 'b', 'n'],
        'h': ['t', 'y', 'u', 'g', 'j', 'b', 'n', 'm', 'k'],
        'i': ['u', 'o', 'j', 'k', 'l', 'p', 'y', 'h'],
        'j': ['y', 'u', 'i', 'h', 'k', 'n', 'm'],
        'k': ['u', 'i', 'o', 'j', 'l', 'm', 'p'],
        'l': ['i', 'o', 'p', 'k', 'j', 'u', 'm','h'],
        'm': ['n', 'h', 'j', 'k', 'l', 'b', 'h','g'],
        'n': ['b', 'g', 'h', 'j', 'm', 'k', 'l', 'v'],
        'o': ['i', 'k', 'l', 'p', 'u', 'j'],
        'p': ['o', 'l', 'i', 'k', 'u', 'j'],
        'q': ['a', 's', 'w', 'e', 'd', 'r', 'f'],
        'r': ['e', 'd', 'f', 'g', 't', 'w', 's', 'y'],
        's': ['q', 'w', 'e', 'a', 'd', 'z', 'x'],
        't': ['r', 'y', 'f', 'g', 'h', 'e', 'd', 'u'],
        'u': ['y', 'i', 'h', 'j', 'k', 'o', 'l', 't', 'g'],
        'v': ['d', 'f', 'g', 'c', 'b', 'h', 'n'],
        'w': ['q', 'e', 'a', 's', 'd', 'r', 'f'],
        'x': ['a', 's', 'd', 'z', 'c', 'f','v'],
        'y': ['t', 'u', 'g', 'h', 'j', 'r', 'f'],
        'z': ['a', 's', 'x', 'd', 'c', 'f']
    }

    def __init__(self):
        """Initialization"""
        self.alphabets = string.ascii_lowercase #alphabets from a to z
        #26 by 26 matrices
        self.emission_matrix = np.zeros((len(self.alphabets), len(self.alphabets)), dtype=int) #array containing count for emission, initially zeros
        self.transition_matrix = np.zeros((len(self.alphabets), len(self.alphabets)), dtype=int) #array containing count for transition, initially zeros
        self.prob_emission_matrix = np.zeros((len(self.alphabets), len(self.alphabets)), dtype=float) #array containing emission probabilities
        self.prob_transition_matrix = np.zeros((len(self.alphabets), len(self.alphabets)), dtype=float) #array containing transition probabilities

        self.prepareData()

    def readFromFile(self,filename):
        """Open file for reading text data"""
        with open(filename,'rt') as in_file:
            whole_doc = in_file.read()
            return whole_doc

    def splitDoc(self,whole_doc):
        """splitting the document to 80 % training set and 20 % test set"""
        word_list = re.findall(r'\w+', whole_doc)
        index_split = int(0.8 * len(word_list))
        self.training_set = [item.lower().strip() for item in word_list[:index_split] if item.isalpha()]
        self.testing_set = [item.lower().strip() for item in word_list[index_split:] if item.isalpha()]

    def calculateProbabilityEmission(self):
        """calculate emission probability"""
        for i in range(0, len(self.alphabets)):
            # If there's a zero count in a item array, then do smoothing(add 1 to every element)
            if (0 in self.emission_matrix[i]):
                self.emission_matrix[i] = [x+1 for x in self.emission_matrix[i]]
            # add all the elements of the letter array
            sum = self.emission_matrix[i].sum()
            for j in range(0, len(self.alphabets)):
                self.prob_emission_matrix[i][j] = float(self.emission_matrix[i][j]) / sum
        return self.prob_emission_matrix

    def calculateProbabilityTransition(self):
        """calculate transition probabilty"""
        for i in range(0, len(self.alphabets)):
            # Do Smoothing
            if (0 in self.transition_matrix[i]):
                self.transition_matrix[i] = [x+1 for x in self.transition_matrix[i]]
            sum = self.transition_matrix[i].sum()
            for j in range(0, len(self.alphabets)):
                self.prob_transition_matrix[i][j] = float(self.transition_matrix[i][j]) / sum
        return self.prob_transition_matrix


    def corruptText(self,word_list,isTrainingData):
        """Corrupt word"""
        corrupted_list = []

        for word in word_list:
            corrupted_word = ""

            for i in range(0,len(word)):
                # generate random number between 0 and 1
                random_num = random.uniform(0, 1)

                if(random_num > 0.2):
                    #do not corrupt the letter
                    corrupted_word += word[i].lower().strip()
                else:
                    #corrupt the letter
                    corrupted_word += random.choice(self.mistake_characters[word[i].lower().strip()])

                # update the count for emission probability and transition probability
                if (isTrainingData):
                    #increment emission count
                    self.emission_matrix[self.alphabets.index(word[i].lower().strip())][
                        self.alphabets.index(corrupted_word[i].lower())] += 1

                    # Keep track of the transitions from state i to state j
                    if (i is not len(word) - 1):
                        # count the transition from state i to state j
                        self.transition_matrix[self.alphabets.index(word[i].lower().strip())][
                            self.alphabets.index(word[i + 1].lower())] += 1

            corrupted_list.append(corrupted_word.strip())

        return corrupted_list


    def prepareData(self):
        #Read from file
        whole_doc = self.readFromFile("unabom.txt")

        #Split document into 20 % for testing and 80 % for training
        self.splitDoc(whole_doc)

        # Corrupt the training set
        self.corrupted_training_set = self.corruptText(self.training_set, True)

        # Corrupt the test set
        self.corrupted_testing_set = self.corruptText(self.testing_set, False)

        #calculate the emission probabilty matrix
        self.prob_emission_matrix = self.calculateProbabilityEmission()

        #calculate the transition matrix probability matrix
        self.prob_transition_matrix = self.calculateProbabilityTransition()


print "Processing..."
spellingCorrectorObj = SpellingCorrector()
hmmObj = Hmm.HMM(spellingCorrectorObj.prob_emission_matrix, spellingCorrectorObj.prob_transition_matrix, spellingCorrectorObj.corrupted_testing_set, spellingCorrectorObj.testing_set)
