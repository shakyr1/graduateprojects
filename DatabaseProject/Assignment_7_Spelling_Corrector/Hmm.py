import math
import string

class HMM:
    def __init__(self, prob_emission, prob_transition, corrupted_test_set,testing_set):
        """Initialization"""
        self.delta = []
        self.states = list(string.ascii_lowercase) #states a to z
        self.symbols = list(string.ascii_lowercase) #symbols a to z
        self.prob_emission = prob_emission #emission matrix
        self.prob_transition = prob_transition #transition matrix
        self.corrupted_test_set = corrupted_test_set
        self.initial_probability = float(1)/len(self.states) # Same initial probability for every states
        self.test_set = testing_set

        self.False_Negative = 0 #wrong -> wrong
        self.True_Positive = 0 #wrong -> correct
        self.False_Positive = 0 #correct -> wrong
        self.True_Negative = 0 #correct -> correct
        self.total_test_words = 0

        # start processing
        self.main(self.test_set)


    def calculateDeltaOne(self, first_symbol):
        """Calculate Delta for first symbol"""
        self.delta = [] #initializing to empty for every word
        for index in range(0, len(self.states)):
            temp_emission_prob = self.prob_emission[index][self.symbols.index(first_symbol)]
            self.delta.append(math.log(temp_emission_prob) + math.log(self.initial_probability))

    def calculateDeltaAndPsi(self, letter):
        """Calculate Delta for rest of the symbols and calculate the psi list"""
        psi = [None] * len(self.states) #list of states at which the values are max
        temp_delta = [None] * len(self.states) #To track the intermediate delta
        for j in range(0, len(self.states)):
            max_val = None
            for i in range(0, len(self.states)):
                temp = self.delta[i] + math.log(self.prob_transition[i][j])
                if (temp > max_val or max_val is None):
                    max_val = temp
                    psi[j] = self.symbols[i]
            temp_delta[j] = max_val + math.log((self.prob_emission[j][self.symbols.index(letter)]))
        self.delta = temp_delta
        return psi

    def wordCorrection(self, psi_list):
        #back tracking for the best path
        max_delta = max(self.delta)
        temp = self.delta.index(max_delta)
        word = [self.symbols[temp]]
        for element in psi_list:
            word.append(element[temp])
            temp = self.states.index(element[temp])
        word.reverse()
        return ''.join(word)


    def calculateRecall(self):
        """calculate recall"""
        return float(self.True_Positive) / (self.True_Positive + self.False_Negative) * 100

    def calculatePrecision(self):
        """calculate precision"""
        return float(self.True_Positive) / (self.True_Positive + self.False_Positive) * 100

    def computeRecallPrecisionParameters(self,original_word,corrupted_word,corrected_word):
        if (original_word != corrupted_word):
            # if the correct word has been corrupted, check if the corrupted word is corrected or not
            if (original_word == corrected_word):
                # if the corrupted word i corrected
                # TP: True Positive (wrong->correct)
                self.True_Positive += 1
            else:
                # if the corrupted word is not corrected
                # FN: False Negative (wrong->wrong)
                self.False_Negative += 1

        else:
            if (corrupted_word != corrected_word):
                # FP: False Positive (correct->wrong)
                self.False_Positive += 1
            else:
                #TN: True Negative (correct->correct)
                self.True_Negative += 1

    def showResult(self):
        print "============================================"
        print "Ronish Shakya"
        print "Assignment 7 - Spelling Correction"
        print "============================================"
        print "Total Words for Testing        :" + str(self.total_test_words)
        print "True Positive  (Wrong->Correct):" + str(self.True_Positive)
        print "False Negative   (Wrong->Wrong):" + str(self.False_Negative)
        print "False Positive (Correct->Wrong):" + str(self.False_Positive)
        # print "True Negative(Correct->Correct):" + str(self.True_Negative)
        print "============================================"
        print "Recall: " + str(self.calculateRecall()) +" %"
        print "Precision: " + str(self.calculatePrecision()) +" %"
        print "============================================"

    def main(self,test_set):
        index = 0 #index for test_set
        for corrupted_word in self.corrupted_test_set:
            psi_list = []
            for i in range(0, len(corrupted_word)):
                symbol_char = corrupted_word[i]
                if (i == 0):
                    #Calculate Delta one
                    self.calculateDeltaOne(symbol_char)
                else:
                    #insert the psi array at the beginning of list
                    tempPsi = self.calculateDeltaAndPsi(symbol_char)
                    psi_list.insert(0, tempPsi)
            #correct the corrupted word
            corrected_word = self.wordCorrection(psi_list)
            self.total_test_words += 1

            #Compute TP,FP and FN
            self.computeRecallPrecisionParameters(test_set[index],corrupted_word,corrected_word)
            index += 1
        self.showResult()
