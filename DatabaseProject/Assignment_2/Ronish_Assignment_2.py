#Ronish Shakya
#Assignment 2

from nltk.corpus import stopwords
from nltk.tokenize import wordpunct_tokenize
from nltk.stem.porter import PorterStemmer
from itertools import groupby


with open("cran.all.1400", 'rt') as in_file:  # Open file for reading text data
    whole_doc = in_file.read()

id_count = 0
doc_list = []
porter = PorterStemmer()
print "\nRonish Shakya"
print "Assignment #2"
print "*************\n"
for word in whole_doc.split():
    if word == ".I":
        id_count = id_count + 1
        begin_pos = '.I '+str(id_count)
        final_pos = '.I '+str(id_count+1)
        data = whole_doc[whole_doc.find(begin_pos)+len(''):whole_doc.find(final_pos)].strip()
        data = data[data.find('.W') + len(''):data.find('.I ' + final_pos)].strip()
        # remove stop words
        data = data.replace(".W", "")
        stop_words = set(stopwords.words('english'))
        stop_words.update(['.', ',', '"', "'", '?', '!', ':', ';', '(', ')', '[', ']', '{', '}', '/', '-'])
        list_of_words = [porter.stem(i.lower()) for i in wordpunct_tokenize(data) if i.lower() not in stop_words]

        # finding the term frequency

        freq_dict = dict((key, len(list(group))) for key, group in groupby(sorted(list_of_words)))

        # calculating no. of unique terms
        word_set = set(list_of_words)
        print "Document ID: " + str(id_count)
        print "Number of unique terms: " + str(len(word_set))
        print "(Term,Frquency)"
        # printing frequency of the terms
        for i in sorted(freq_dict):
            print "(" + i + "," + str(freq_dict[i]) + ")"

        print "\n"




