#Ronish Shakya
#Assignment 5
import json
import re
import math
from stemmer import PorterStemmer


def readFile(fileName, type):
    """Returns the content of the file."""
    file = open(fileName, type)
    fileContent = file.read()
    return fileContent

def loadInvertedIndex():
    """Load the Inverted index"""
    with open('output.json', 'r') as f:
        return json.load(f)

def getQueryList(queryFileRead):
    """Returns the query list"""

    tempQueryList = re.split(".I | \n.I", queryFileRead)[1:]
    queryList = []
    for query in enumerate(tempQueryList):
        startIndex = query[1].index('.W')
        text = query[1][startIndex + 3:]
        queryList.append(text)

    return queryList

def calculateIDF(inverted_index, total):
    """Calculate IDF of each term."""
    IDF = {}
    for item in inverted_index:
        IDF[item] = {'freq': inverted_index[item][0], 'idf': math.log(1 + total / inverted_index[item][0]), 'docList': inverted_index[item][1]}
    return IDF

def calculateTFList(inverted_index):
    """Calculate Term Frequency list of every words"""
    TF = {}
    for item in inverted_index:
        TF[item] = []
        for data in inverted_index[item][1]:
            TF[item].append([data[0], 1 + math.log(data[1])])
    return TF

def calculateWD(TF, totalDocument):
    """Calculate the Wd"""
    WD = {}
    for docId in range(1, totalDocument + 1):
        tempWd = 0
        for item in TF:
            listItem = [x[0] for x in TF[item]]
            if (docId in listItem):
                indexDocId = listItem.index(docId)
                tempWd += math.pow(TF[item][indexDocId][1], 2)
        WD[docId] = math.sqrt(tempWd)
    return WD

def calculateSimilarity(processedQuery, IDF, WD, totalDocument):
    Similarity = {}
    for docId in range(1, totalDocument + 1):
        temp = 0
        # To skip those documents that are blank
        if not WD[docId]:
            continue
        for item in processedQuery:
            if (item in IDF):
                listItem = [x[0] for x in IDF[item]['docList']]
                if docId in listItem:
                    indexDoc = listItem.index(docId)
                    freqDT = IDF[item]['docList'][indexDoc][1]
                    logFreqDT = 1 + math.log(freqDT)
                else:
                    logFreqDT = 0
                temp += logFreqDT * IDF[item]['idf']
            else:
                temp += 0
        if (temp):
            Similarity[docId] = 1 / WD[docId] * temp

    sortedDocument, sortedRank = sorted(Similarity, key=Similarity.__getitem__, reverse=True), sorted(Similarity.values(), reverse=True)
    return sortedDocument

def vectorSpace(querydoc, stopwords):

    # load inverted index created in previous assignment
    inverted_index = loadInvertedIndex()
    # get query list
    queryList = getQueryList(querydoc)
    # Calculation of Inverse Document Frequency
    IDF = calculateIDF(inverted_index, 1400)
    # Calculation of Term Frequency
    TF = calculateTFList(inverted_index)
    # Calculation of Wd from all the Term Frequency calculated
    WD = calculateWD(TF, 1400)

    porter_stemmer = PorterStemmer()
    fileWrite = open("Result.txt", "w")

    print "Writing the final result into Result.txt"

    for query in queryList:
        fileWrite.write("\n")
        fileWrite.write("\nQuery: " + query)
        # Separate the string of query into list of words
        listQuery = re.findall(r'\w+', query)
        # Remove the stopwords and numbers from the list of query words
        queryWithoutStopword = [x for x in listQuery if x not in stopwords and x.isalpha()]
        # Stem the list of query words
        processedQuery = [porter_stemmer.stem(x.lower(), 0, len(x) - 1) for x in queryWithoutStopword]
        # Calculate the cosine measure (Similarity) for the query
        rankedDocList = calculateSimilarity(processedQuery, IDF, WD, 1400)
        fileWrite.write("\nTotal number of documents retrieved: " + str(len(rankedDocList)))
        fileWrite.write("\nDocument Ranking:\n")
        fileWrite.write(''.join(str(rankedDocList)))
        fileWrite.write("\n")
    fileWrite.close()

    print "The result is written into Result.txt"


def main():
    """main function"""
    # Read stop words from file
    stopwordsList = readFile("stopwords.txt", "r")
    stopwords = stopwordsList.split()

    # Read the queries list from the cran query file
    querydoc = readFile("cran.qry", "r")

    #vector space model
    vectorSpace(querydoc,stopwords)



main()